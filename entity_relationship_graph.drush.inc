<?PHP

/**
 * Implements hook_drush_command().
 */
function entity_relationship_graph_drush_command() {

  $items['entity-relationship-graph-create'] = array(
    'description' => 'Create DOT code.',
    'aliases' => array('ergc'),
    'examples' => array(
      'drush ergc | dot -Tpng > erg.png' => 'Generate the Entity relationship graph for the current site and export it has a PNG image.',
    ),
  );

  return $items;
}


/**
 * Create DOT code of all entity references.
 */
function drush_entity_relationship_graph_create() {

  $edges = array();

  // Debug code.
  /*
  $i = field_info_instances('comment', 'comment_node_article');
  drush_print_r($i);
  return;
  $i = field_info_extra_fields('node', 'shortcut', 'form');
  drush_print_r($i);
  return;
  $i = field_info_field('field_example');
  drush_print_r($i);
  return;
  $field_instance_infos = field_info_instances('node', 'press_release');
  drush_print_r($field_instance_infos);
  return;
*/



  $entity_infos = entity_get_info();
  //drush_print_r($entity_infos);
  //return;
  //unset($entity_infos['user']);
  //unset($entity_infos['file']);

  // Begin graph.
  drush_print('
digraph content_structure { 
  node [
    shape = "none"
  ];  
  ');

  foreach ($entity_infos as $entity_name=>$entity_info) {

    // A cluster subgraph for every entity type.
    drush_print('subgraph cluster_' . $entity_name . ' {');
    drush_print('style=filled;');
    drush_print('color=grey90;');
    drush_print_r('label = <<FONT COLOR="red">' . $entity_name . '</FONT>>');

    foreach ($entity_info['bundles'] as $bundle_name=>$bundle_info) {

      // A node for every bundle.
      drush_print('"' . $entity_name . '_' . $bundle_name . '"');
      drush_print('[
        label =<
          <TABLE BORDER="0" CELLBORDER="0" CELLSPACING="0" CELLPADDING="5">
            <TR ><TD COLOR="white" bgcolor="black"  PORT="__title"><FONT COLOR="white">' . $bundle_name . '</FONT></TD></TR>
      ');

      $field_info_instances = field_info_instances($entity_name, $bundle_name);
      foreach ($field_info_instances as $field_instance_name=>$field_instance_info) {

        // A line in the node for every field.
        $port = 'field_instance_' . $field_instance_info['id'];
        $bgcolor = '#a0a0a0';
        $color = 'black';
        $field_info = field_info_field($field_instance_name);

        // Special treatment of certain field types.
        switch ($field_info['type']) {

        case 'entityreference':
          foreach ($field_info['settings']['handler_settings']['target_bundles'] as $target_bundle) {
            // Connect entityreferences.
            $edges[$entity_name . '_' . $bundle_name . ':' . $port][] = $field_info['settings']['target_type'] . '_' . $target_bundle . ':__title';
            $bgcolor = '#F9966B';
            $color = 'white';
          }
          break;

        case 'field_collection':
          // Connect field_collections.
          $edges[$entity_name . '_' . $bundle_name . ':' . $port][] = 'field_collection_item_' . $field_instance_name . ':__title';
          break;

        case 'taxonomy_term_reference':
          foreach ($field_info['settings']['allowed_values'] as $allowed_value) {
            // Connect term references.
            $edges[$entity_name . '_' . $bundle_name . ':' . $port][] =  'taxonomy_term_' . $allowed_value['vocabulary'] . ':__title';
            $bgcolor = '#ADD8E6';
          }
          break;
        }
        drush_print('
              <TR><TD bgcolor="' . $bgcolor . '" PORT="' . $port . '"><FONT COLOR="' . $color . '">' . $field_instance_name . '</FONT></TD></TR>
        ');
      }

      // Show also extra fields
      // (like title, metatag...).
      $extra_fields = field_info_extra_fields($entity_name, $bundle_name, 'form');
      foreach ($extra_fields as $extra_field_name=>$extra_field) {
        $port = $entity_name . '_' . $bundle_name . '_' . $extra_field_name;
        $bgcolor = '#c0c0c0';
        $color = 'black';
        drush_print('
              <TR><TD bgcolor="' . $bgcolor . '" PORT="' . $port . '"><FONT COLOR="' . $color . '">' . $extra_field_name . '</FONT></TD></TR>
        ');
      }

      drush_print('
          </TABLE>
        > ];
      ');

      if ($entity_name == 'comment') {
        // Connect comments.
        $edges[$entity_name . '_' . $bundle_name . ':__title'][] = 'node_' . $bundle_info['node bundle'] . ':__title';
      }

    }
    drush_print('}');
  }

  // Add collected edges of the connections.
  foreach ($edges as $source=>$destinations) {
    foreach ($destinations as $destination) {
      drush_print($source . ' -> '. $destination . ';');
    };
  };

  drush_print('}');
}
